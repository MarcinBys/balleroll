﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;

    private static bool win;
    private static bool lose;

    [Range(0,10)] public float smoothing;
    private Vector3 currentAcceleration, initialAcceleration;

    public Text endText;



    void Start()
    {
        win = false;
        lose = false;

        endText.text = "";

        rb = GetComponent<Rigidbody2D>();

        initialAcceleration = Input.acceleration;
        currentAcceleration = Vector3.zero;
    }



    void Update()
    {
        if (!win && !lose)
        {
            currentAcceleration = Vector3.Lerp(currentAcceleration, Input.acceleration - initialAcceleration, Time.deltaTime / smoothing);
            transform.Translate(currentAcceleration.x / 10, currentAcceleration.y / 10, 0);
        }
        else if (win)
        {
            endText.text = "Wygrana";
            gameObject.transform.localScale = Vector3.zero;
            rb.velocity = Vector3.zero;
        }
        else if (lose)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }



    public static void Lose()
    {
        lose = true;
    }



    public static void Win()
    {
        win = true;
    }
}
