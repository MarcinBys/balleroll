﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SceneManage : MonoBehaviour
{
    public GameObject loadingSceenObject;
    public Slider slider;
    public GameObject loadingSpiral;

    private GameObject[] UIToHide;


    /// <summary>
    /// 
    /// </summary>
    /// <param name="levelName"></param>
    public void LoadLevel(string levelName)
    {
        UIToHide = GameObject.FindGameObjectsWithTag("Button");

        foreach (GameObject obj in UIToHide)
        {
            obj.SetActive(false);
        }

        StartCoroutine(LoadAsynchronous(levelName));
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="levelName"></param>
    /// <returns></returns>
    IEnumerator LoadAsynchronous(string levelName)
    {
        loadingSceenObject.SetActive(true);

        //loadingSpiral.transform.Rotate(new Vector3(0, 0, 45));

        AsyncOperation operation = SceneManager.LoadSceneAsync(levelName);
        operation.allowSceneActivation = false;

        while (!operation.isDone)
        {
            slider.value = operation.progress;

            if (operation.progress.Equals(0.9f))
            {
                slider.value = 1f;
                operation.allowSceneActivation = true;
            }

            /*
            bool pressed = false;
            
            while (!pressed)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase.Equals(TouchPhase.Began))
                {
                    operation.allowSceneActivation = true;
                }
            }*/

            yield return null;
        }
    }


    // ================= QUICK LOADING WITHOUT LOADING SCREEN =========================

    /// <summary>
    /// Load scene without loading progress bar
    /// </summary>
    /// <param name="sceneName"></param>
    public void QuickLoadScene(string sceneName)
    {
        Debug.Log("Loading scene: " + sceneName);
        SceneManager.LoadScene(sceneName);
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }

}
