﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGenerator : MonoBehaviour
{
    public GameObject holes;
    public int holesAmount;

    private void Awake()
    {
        GenerateScene(holesAmount, holes);
    }

    public void GenerateScene(int objectAmount, GameObject objectToSpawn)
    {
        for (int i = 0; i < objectAmount; i++)
        {
            Vector3 objectPosition = new Vector3(Random.Range(-12, 12), Random.Range(-7, 7), 0);

            if (!Physics2D.OverlapCircle(objectPosition, 1))    // if there is nothing at this position
            {
                Instantiate(objectToSpawn, objectPosition, new Quaternion());
            }
            else                                                // if there already exists something in this position
            {
                i--;
            }
        }
    }
}
